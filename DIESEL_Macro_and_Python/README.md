# DIESEL Macro + Python
DISELマクロとPython（など）を組み合わせて、一連の操作をなるべくスムーズにひとつのコマンドであるかのように動作させる。

## 実現すること
以下の一連の操作をひとまとめにします。

1. オブジェクトを選択する。
1. DXFOUT実行。
1. 選択したオブジェクトを削除。
1. PythonスクリプトでDXFファイルを編集。
1. その編集されたDXFファイルをブロック挿入。
1. ブロックを分解する。
1. ブロックを名前削除する。
1. （DXFファイルを削除する。）

#### Release版向けテンプレート
```
^C^C_SELECT;$M=$(if,$(getvar,cmdactive),\)_DXFOUT;"DXFファイルのパス";O;P;;V;2013;16;_ERASE;P;;(acet-sys-command "スクリプトランチャーのパス");-INSERT;"DXFファイルのパス";0,0;1;1;0;_EXPLODE;L;-PURGE;B;ブロック名;N^M
```
※Pythonスクリプトの実行終了まで待ちます。

#### LT版向けテンプレート
```
^C^C_SELECT;$M=$(if,$(getvar,cmdactive),\)_DXFOUT;"DXFファイルのパス";O;P;;V;2013;16;_ERASE;P;;AI_STARTAPP;"スクリプトランチャーのパス";DELAY;2000;-INSERT;"DXFファイルのパス";0,0;1;1;0;_EXPLODE;L;-PURGE;B;ブロック名;N^M
```
※Pythonスクリプトの実行終了まで待たずに次のコマンドに進むため2秒の待ち時間を取ります。時間を更に調整するか、DISELマクロをふたつに分ける必要があるかもしれません。

### 作例

#### 置き換え部分
+ DXFファイルのパス: c:/temp/edit_dxf.dxf
+ スクリプトランチャーのパス: c:/bin/run_py.bat
+ ブロック名: edit_dxf
+ 掃除用スクリプトのパス: ********

#### スクリプトランチャーの内容
以下のように記述しているものとします。
```.bat
@echo off
python.exe "%~dp0dxf_mtext_zenhan.py" "c:\temp\edit_dxf.dxf"
```

#### Release版向けの作例
```
^C^C_SELECT;$M=$(if,$(getvar,cmdactive),\)_DXFOUT;"c:/temp/edit_dxf.dxf";O;P;;V;2013;16;_ERASE;P;;(acet-sys-command "c:/bin/run_py.bat");-INSERT;"c:/temp/edit_dxf.dxf";0,0;1;1;0;_EXPLODE;L;-PURGE;B;edit_dxf;N^M
```

#### LT版向けの作例
```
^C^C_SELECT;$M=$(if,$(getvar,cmdactive),\)_DXFOUT;"c:/temp/edit_dxf.dxf";O;P;;V;2013;16;ERASE;P;;AI_STARTAPP;"c:/bin/run_py.bat";DELAY;2000;-INSERT;"c:/temp/edit_dxf.dxf";0,0;1;1;0;_EXPLODE;L;-PURGE;B;edit_dxf;N^M
```
