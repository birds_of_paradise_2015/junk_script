# -*- coding: UTF-8 -*-
""" 
再付番実行プログラム

動作確認：
    Python 3.6.4
    ezdxf 0.8.3
################################################################################

The MIT License (MIT)

Copyright (c) 2018 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from __future__ import print_function

import os
import ezdxf

import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox


class Dialog:
    """再付番実行プログラム用のダイアログ"""

    def __init__(self, master):

        # 作業するファイルダイアログボックス用オプション設定
        base_dir = os.path.abspath(os.path.dirname(__file__))
        self.file_opt = options = {}
        options['filetypes'] = [('DXFファイル', '*.dxf')]
        options['initialdir'] = base_dir
        options['parent'] = root
        options['title'] = '作業するファイルの選択'

        frame = tk.Frame(master)
        frame.pack()

        master.title('再付番実行プログラム')
        # master.geometry('300x100')

        self.label1 = tk.Label(frame, text='作業するファイル：')
        self.label1.pack(side=tk.TOP)

        self.edit_path = tk.Entry(frame, width=50)
        self.edit_path.pack(side=tk.TOP)

        self.browse_button = tk.Button(frame, text='参照', command=self.browse)
        self.browse_button.pack(side=tk.TOP)

        self.run_button = tk.Button(frame, text='実行', fg='red', command=self.run)
        self.run_button.pack(side=tk.LEFT)

        self.close_button = tk.Button(frame, text='閉じる', command=frame.quit)
        self.close_button.pack(side=tk.RIGHT)

    def browse(self):
        """ファイル選択用ダイアログボックスを表示する。"""
        directory = tkinter.filedialog.askopenfilename(**self.file_opt)
        self.edit_path.delete(0, tk.END)
        self.edit_path.insert(tk.END, directory)

    def run(self):
        """再付番の実行。"""
        dxffile = self.edit_path.get()
        if not os.path.exists(dxffile):
            tkinter.messagebox.showwarning('警告', u'{0:s} ファイルが見つかりません。'.format(dxffile))
            return

        try:
            self.renumber(dxffile)
        except IndexError as e:
            tkinter.messagebox.showinfo('情報', '再付番するTEXTがありませんでした。')
            return

        tkinter.messagebox.showinfo('情報', '処理を終了しました。')

    def renumber(self, dxffile):
        """
        図面の数字を再付番します。

        マルチテキストには非対応。
        """
        dwg = ezdxf.readfile(dxffile)

        modelspace = dwg.modelspace()

        # 図面から数字のみで構成されているTEXTを抽出する。
        texts = modelspace.query('TEXT[text ? "\d{1,}"]')

        # 並び替え用に数字のリストを作成する。
        numbers = [int(text.get_dxf_attrib('text')) for text in texts]

        sorted_numbers = sorted(list(set(numbers))) # 重複は削除する。

        # 最小値を基準として、１づつ加算する。
        base_number = sorted_numbers[0]
        for num in sorted_numbers:
            # 再付番するTEXTを検索する。
            ents = texts.query('TEXT[text ? "{}"]'.format(num))
            # 同じ数字は同じ値で上書きする。
            for ent in ents:
                ent.set_dxf_attrib('text', base_number)
                # print('{} => {}'.format(ent.get_dxf_attrib('text'), base_number))
            base_number += 1

        dwg.save()


if __name__ == '__main__':
    root = tk.Tk()
    my_dialog = Dialog(root)
    root.mainloop()
