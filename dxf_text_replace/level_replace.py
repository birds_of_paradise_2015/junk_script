# coding: utf-8
#
import os
from pathlib import Path
import re
import ezdxf


class LevelReplacer(object):
    """ レベルを指定した値に書き換える """

    def __init__(self):
        """ 初期化 """
        self.dwg = None
        self.repleace_value = 500.0

    def calc_level(self, level: float) -> float:
        """ 新たなレベルの計算 """
        return level + self.repleace_value

    def parse_dxf_text(self, text: str) -> tuple:
        """ レベル文字列の解析。文字列部分と数字部分に分ける。 """
        value = re.search('\A([\d|R]FL)([+-]\d+(?:\.\d+)?)\Z', text)
        return value.groups()

    def level_replacer(self, dxffile: str) -> None:
        """ レベルを指定した値に書き換える """
        self.dwg = ezdxf.readfile(dxffile)
        drawing = self.dwg.modelspace()

        level_texts = drawing.query('TEXT[layer ? ".+高.+"]')

        for level_text in level_texts:
            print(f'LAYER: <{level_text.dxf.layer}>, TEXT: {level_text.dxf.text}')
            try:
                level_name, level_value = self.parse_dxf_text(level_text.dxf.text)
                level_text.dxf.text = f'{level_name}{self.calc_level(float(level_value)):+g}'
                print(f'-> {level_name}{self.calc_level(float(level_value)):+g}')
            except AttributeError:
                pass

        self.dwg.saveas(dxffile.replace('.dxf', '_after.dxf'))


if __name__ == '__main__':
    pass
