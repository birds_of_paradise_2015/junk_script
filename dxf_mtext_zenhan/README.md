# dxf_mtext_zenha

全角英数字を半角英数字に置き換える。

---

## 使い方
コマンドプロンプトから、

```
python dxf_mtext_zenha.py xxxxx.dxf
```

とタイプして実行する。

xxxxx.dxfは編集するDXFファイルになります。

## 依存ライブラリ
- ezdxf
- jaconv
