# coding: utf-8
""" 
全角英数字を半角英数字に置き換える。

-----

動作確認：
    Python 3.7.3
    ezdxf 0.9
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import sys
from pathlib import Path
import ezdxf

use_jaconv = False
try:
    import jaconv
    use_jaconv = True
except ImportError:
    # jaconvがなければ、unicodedataを使う。
    import unicodedata

class MTextZenToHan(object):
    """ 全角英数字を半角英数字に置き換える。

        マルチテキストとダイナミック文字に対応。
        マルチテキストは書式を考慮していない。
    """

    def __init__(self):
        """ 初期化 """
        self.dwg = None

    def zenhan(self, dxffile):
        """ 全角英数字を半角英数字に置き換える """
        self.dwg = ezdxf.readfile(dxffile)
        drawing = self.dwg.modelspace()

        mtexts = drawing.query('MTEXT TEXT')

        for mtext in mtexts:
            if mtext.dxftype() == 'MTEXT':
                if use_jaconv:
                    mtext.set_text(jaconv.z2h(mtext.get_text(), kana=False, digit=True, ascii=True))
                else:
                    mtext.set_text(unicodedata.normalize('NFKC', mtext.get_text()))
            else: # TEXT
                if use_jaconv:
                    mtext.dxf.text = jaconv.z2h(mtext.dxf.text, kana=False, digit=True, ascii=True)
                else:
                    mtext.dxf.text = unicodedata.normalize('NFKC', mtext.dxf.text)

        # self.dwg.saveas(dxffile.replace('.dxf', '_after.dxf'))
        self.dwg.save()


if __name__ == '__main__':
    dxffile = Path(sys.argv[1])
    test_dxf = MTextZenToHan()
    if dxffile.exists():
        test_dxf.zenhan(dxffile.resolve())
