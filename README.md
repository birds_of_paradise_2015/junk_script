# junk script

サンプルコードなど。

---

## DIESEL_Macro_and_Python
DISELマクロとPython（など）を組み合わせて利用するためのドキュメント。

## connect_o_and_f
DXFファイルからFIELDデータを探索・表示する。

## dxf_add_bracket
TEXTの前後に括弧を追加します。

## dxf_chblocks
ブロック内のエンティティの属性を変更する。
色、線種、線の太さを「BYBLOCK」に変更する。

## dxf_collectdim2xl
図面内の寸法をExcelに書き出す。（要openpyxl）

## dxf_delete_layouts
図面から指定したレイアウトタブ(2ページ目と5ページ目以降)を削除する。

## dxf_dimtext_copy
複数選択された寸法に対して、一番目の「寸法値の優先」の値を二番目以降の寸法に上書きする。

## dxf_hex_counter
１６進数カウンタ

## dxf_integrate_layer
各画層を画層「0」に統合する。

## dxf_mtext_zenha
全角英数字を半角英数字に置き換える。

## dxf_replace_block_name
挿入されている特定のブロック名を変更します。

## dxf_text_replace
レベルを指定した値に書き換える。

## make_lin
dxfファイルから線種ファイルを作成する。

## renumber
図面内の数字を再付番する（マルチテキストには非対応）。
