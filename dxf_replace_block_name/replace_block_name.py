# coding: utf-8
""" 
特定のブロック名を変更する。

-----

動作確認：
    Python 3.7.4
    ezdxf 0.9
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import ezdxf

# pyperclipが利用可能あれば、クリップボードに
# ブロック名のリストをコピーするためにインポートする。
try:
    import pyperclip
    use_pyperclip = True
except ImportError:
    use_pyperclip = False


class ReplaceBlockName(object):
    """ 特定のブロック名を変更する。 """

    def __init__(self, dxffile):
        """ 初期化 """
        self.dwg = None
        self.save_file = None
        # ベースになるブロック名
        self.base_block = None
        # 図面内のブロック名のリスト
        self.blocks = None
        # 候補になるブロック名のリスト
        self.candidate_blocks = None
        # 置き換えるブロック名のリスト
        self.replace_blocks = None

        self.dwg = ezdxf.readfile(dxffile)

    def save(self):
        """ 図面を保存する """
        self.dwg.save()

    def saveas(self):
        """ 図面を名前を付けて保存する """
        """ 今のところ末尾に「_after」を足すだけ """
        self.save_file = self.dwg.filename.replace('.dxf', '_after.dxf')
        self.dwg.saveas(self.save_file)

    def get_blocks(self):
        """ 図面内のブロック名をリスト化する """
        return [block.name for block in self.dwg.blocks]

    def generate_blocks(self):
        """ self.blocks ブロック名のリストを生成する """
        """ 今のところ、何も加工していない """
        self.blocks = self.get_blocks()

    def generate_candidate_blocks(self, base_block):
        """ self.blocks ブロック名のリストを生成する """
        """ 今のところ、何も加工していない """
        self.candidate_blocks = self.blocks.copy()
        self.candidate_blocks.remove(base_block)

    def copy_to_clipboard(self, texts):
        """ tuple/listを連結してstringにしてクリップボードにコピーする """
        """ pyperclipがない場合は何もしない """
        if use_pyperclip:
            pyperclip.copy("\r\n".join(texts))

    def replace_block_name(self):
        """ ブロック名を変更する """
        # クリップボードにブロック名のリストをコピーする（pyperclipがある場合）
        self.copy_to_clipboard(self.get_blocks())

        # 特定のブロックだけ変更する場合は、
        # BlocksSection.get(name, default=None)
        # EntityQuery.query(query='*')
        # を使用する。
        # https://ezdxf.mozman.at/docs/blocks/blocks.html
        # https://ezdxf.mozman.at/docs/query.html
        
        entitys = self.dwg.query('INSERT')

        # queryを使ってさらに絞り込む場合は、以下のように記述する。
        # この場合、if文は不要になる。
        # entitys = self.dwg.query('INSERT[name=="block_C"]')
        
        for entity in entitys:

            # ブロック名がreplace_blocksにあるものと一致していれば
            # ブロック名を置き換える。
            # レイヤ名も変更する。
            if entity.dxf.name in self.replace_blocks:
                entity.dxf.name = self.base_block
                # entity.dxf.layer = "layer_A"

if __name__ == '__main__':
    pass
