#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# Support module generated by PAGE version 4.24.1
#  in conjunction with Tcl version 8.6
#    Aug 11, 2019 01:44:46 AM JST  platform: Windows NT

import sys

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

from pathlib import Path

import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox

from replace_block_name import ReplaceBlockName

def set_Tk_var():
    global combobox
    combobox = tk.StringVar()
    global listbox
    listbox = tk.StringVar()

def analyze():
    """DXFファイルを解析してブロック名を取得します。"""

    global w
    global run_instance

    dxffile = w.TEntry1.get()
    if not Path(dxffile).exists():
        tkinter.messagebox.showwarning('警告', f'"{dxffile}"\n\nファイルが見つかりません。')
        return

    try:
        run_instance = ReplaceBlockName(dxffile)
        run_instance.generate_blocks()
        if len(run_instance.blocks) != 0:
            w.TCombobox1['values'] = run_instance.blocks
            w.TCombobox1.current(0)
        else:
            tkinter.messagebox.showwarning('警告', '図面内にブロックが見つかりませんでした。')
            return

    except Exception as e:
        tkinter.messagebox.showinfo('情報', f'エラーが発生しました。\n\n{e}')
        return

def browse():
    """ファイル選択用ダイアログボックスを表示する。"""

    global w, root

    # 作業するファイルダイアログボックス用オプション設定
    base_dir = Path(__file__).resolve().parent
    file_opt = options = {}
    options['filetypes'] = [('DXFファイル', '*.dxf')]
    options['initialdir'] = base_dir
    options['parent'] = root
    options['title'] = '作業するファイルの選択'

    directory = tkinter.filedialog.askopenfilename(**file_opt)
    w.TEntry1.delete(0, tk.END)
    w.TEntry1.insert(tk.END, directory)
    
    # ファイルパスをツールチップで表示する。
    from replace_block_name_gui_take2 import ToolTip
    ToolTip(w.TEntry1, "TkDefaultFont", directory, delay=0.5)

def close():
    destroy_window()

def combobox1_selected(p1):
    global run_instance

    # print(w.TCombobox1.get())
    run_instance.base_block = w.TCombobox1.get()
    run_instance.generate_candidate_blocks(run_instance.base_block)
    listbox.set(run_instance.candidate_blocks)

def entry_enter(p1):
    # print(f'entry is [{w.TEntry1.get()}]')
    # sys.stdout.flush()
    # FIXME: 更新のタイミングがうまく合わない？
    # 合うならanalyze()の内容をここに書きたい。（解析ボタンの削除）
    pass

def listbox_select(p1):
    global run_instance

    run_instance.replace_blocks = w.Scrolledlistbox1.selection_get().split('\n')
    # print(f'=={run_instance.replace_blocks}==')
    # print(f'::{type(run_instance.replace_blocks)}::')

def update():
    """DXFファイルを更新します"""

    global w
    global run_instance

    try:
        run_instance.replace_block_name()
        run_instance.saveas()
    except Exception as e:
        tkinter.messagebox.showinfo('情報', f'エラーが発生しました。\n\n{e}')
        return

    tkinter.messagebox.showinfo('情報', f'処理を終了しました。\n\n{run_instance.save_file}に書き込みました。')

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

if __name__ == '__main__':
    import replace_block_name_gui_take2
    replace_block_name_gui_take2.vp_start_gui()




