# coding: utf-8
""" 
DISEL マクロを作成する。

-----

動作確認：
    Python 3.7.4
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from pathlib import Path
from get_short_path_name import get_short_path_name

# 長いパス。
#
# 作業用DXFファイルのフォルダ部分。
ORIG_DXF_FILE_FOLDER_NAME = r'C:\temp'
# 作業用DXFファイル。
# MS-DOS(8.3)形式とする。
ORIG_DXF_FILE_NAME = r'dxf_temp.dxf'
# 起動用スクリプト。
ORIG_SCRIPT_NAME = r'C:\Users\koh\Documents\Git\junk_script\dxf_dimtext_copy\dxf_dimtext_copy.bat'
# 掃除用スクリプト。
ORIG_SWEEPER_NAME = r'C:\Users\koh\Documents\Git\junk_script\dxf_dimtext_copy\sweeper.bat'

# 待ち時間。1000で約１秒。
DELAY_SECOND = 2000

# DISELマクロでは空白を含むパスを回避する方法がないので、パスはすべて短いパスに変換する。
# セパレータは「\」から「/」に変換する。
#
# 作業用DXFファイル。
DXF_FILE_NAME = get_short_path_name(ORIG_DXF_FILE_FOLDER_NAME).replace('\\', '/') + '/' + ORIG_DXF_FILE_NAME
# 起動用スクリプト。
SCRIPT_NAME = get_short_path_name(ORIG_SCRIPT_NAME).replace('\\', '/')
# 掃除用スクリプト。
SWEEPER_NAME = get_short_path_name(ORIG_SWEEPER_NAME).replace('\\', '/')

# ブロックを分解する際に使用するブロック名（DXFファイル名のベース部分）。
BLOCK_NAME = Path(DXF_FILE_NAME).stem

print(f'DXF_FILE_NAME: {DXF_FILE_NAME}\nSCRIPT_NAME: {SCRIPT_NAME}\nSWEEPER_NAME: {SWEEPER_NAME}\nBLOCK_NAME: {BLOCK_NAME}\n')

STR_SELECT_ENTITY = r'^C^C_SELECT;$M=$(if,$(getvar,cmdactive),\)'
STR_DXFOUT = f'_DXFOUT;"{DXF_FILE_NAME}";O;P;;V;2013;16;'
STR_ERASE = '_ERASE;P;;'
STR_RUN_SCRIPT_R = f'(acet-sys-command "{SCRIPT_NAME}");'
STR_INSERT = f'-INSERT;"{DXF_FILE_NAME}";0,0;1;1;0;'
STR_EXPLODE = '_EXPLODE;L;'
STR_PURGE = f'-PURGE;B;{BLOCK_NAME};N;'
STR_DELETE_TEMP_FILE_R = f'(acet-sys-command "{SWEEPER_NAME}");'

STR_RUN_SCRIPT_LT = f'AI_STARTAPP;"{SCRIPT_NAME}";'
STR_DELAY = f'DELAY;{DELAY_SECOND};'
STR_DELETE_TEMP_FILE_LT = f'AI_STARTAPP;"{SWEEPER_NAME}";'

# Release版向け
STR_DISEL_MACRO_R = STR_SELECT_ENTITY + \
    STR_DXFOUT + \
    STR_ERASE + \
    STR_RUN_SCRIPT_R + \
    STR_INSERT + \
    STR_EXPLODE + \
    STR_PURGE + \
    STR_DELETE_TEMP_FILE_R

# LT版向け
STR_DISEL_MACRO_LT = STR_SELECT_ENTITY + \
    STR_DXFOUT + \
    STR_ERASE + \
    STR_RUN_SCRIPT_LT + \
    STR_DELAY + \
    STR_INSERT + \
    STR_EXPLODE + \
    STR_PURGE + \
    STR_DELETE_TEMP_FILE_LT


if __name__ == '__main__':
    print(f'Release版向け DISEL MACRO({len(STR_DISEL_MACRO_R)}文字):\n{STR_DISEL_MACRO_R}')
    print(f'LT版向け DISEL MACRO({len(STR_DISEL_MACRO_LT)}文字):\n{STR_DISEL_MACRO_LT}')
