# coding: utf-8
""" 
選択された寸法に対して、一番目の「寸法値の優先」の値を二番目以降の寸法に上書きする。

-----

動作確認：
    Python 3.7.4
    ezdxf 0.10.1
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import sys
from pathlib import Path
import ezdxf

class ReplaceDimensionText(object):
    """ 選択された寸法に対して、一番目の「寸法値の優先」の値を二番目以降の寸法に上書きする。

    """

    def __init__(self, dxffile: str) -> None:
        """ 初期化 """
        self.dwg = ezdxf.readfile(dxffile)

    def replace_dimension_text(self) -> None:
        """ 一番目の「寸法値の優先」の値を二番目以降の寸法に上書きする。 """

        dims = self.dwg.query('DIMENSION')

        # 何個選択されているか？
        if len(dims) < 2:
            print('二つ以上選択してください。')
            return

        # 最初の寸法を基準の値とする。
        base_dim = dims.first

        # 「寸法値の優先」の値が定義されているか調べる。
        if not base_dim.has_dxf_attrib('text'):
            print('「寸法値の優先」の値が定義されていません。')
            return

        for dim in dims:
            # print(f'-> [{dim.dxf.text}]')
            dim.update_dxf_attribs({'text': base_dim.dxf.text})

        self.dwg.save()


if __name__ == '__main__':
    dxffile = Path(sys.argv[1])
    rdt_dxf = ReplaceDimensionText(dxffile.resolve())
    rdt_dxf.replace_dimension_text()
