# -*- coding: UTF-8 -*-
""" 
DXFファイルからFIELDデータを探索・表示する。

-----

動作確認：
    Python 3.7
    ezdxf 0.8.8
################################################################################

The MIT License (MIT)

Copyright (c) 2018 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
from pathlib import Path
import ezdxf

class ConnectOAndF(object):
    """
    DXFファイルからFIELDデータを探索・表示する。
    """

    def __init__(self):
        """
        初期化する。
        """
        # 探索済みハンドルのリスト
        self.used_handle = []
        # 図面ファイル
        self.dwg = ''
        # ログ
        self.logs = ''
        self.log_file = ''

    def make_tag_list(self, tag):
        """
        検索するグループコードのリストを作成する。

        コード330,360を抽出する。ハンドルをコレクションする。
        """
        tag_list = []
        if tag.has_tag(330):
            tag_list += tag.find_all(330)
        # if tag.has_tag(331):
        #     tag_list += tag.find_all(331)
        if tag.has_tag(360):
            tag_list += tag.find_all(360)

        return tag_list

    def handle_walk(self, handle):
        """
        ハンドルを元にエンティティを探索する。

        エンティティのタイプとハンドルを表示する。
        """
        tag = ezdxf.lldxf.tags.Tags(self.dwg.entitydb.get(handle))

        tag_list = self.make_tag_list(tag)

        tag_dxf_type = tag.dxftype()
        tag_handle = tag.get_handle()
        if tag_dxf_type not in ('DICTIONARY', 'FIELD', 'BLOCK_RECORD'):
            self.logs += f'TYPE: {tag_dxf_type}, HANDLE: {tag_handle}\n'
        self.used_handle.append(tag_handle)

        try:
            for h in tag_list:
                # 未探索のハンドルであれば、更に探索を進める。
                if h.value not in self.used_handle:
                    self.handle_walk(h.value)
                else:
                    # print('!')
                    return
        except TypeError:
            # print('data end!')
            pass

        return

    def connect_o_and_f(self, dxffile):
        """
        DXFファイルからFIELDデータを探索・表示する。
        """
        self.dwg = ezdxf.readfile(dxffile)

        for ent in self.dwg.objects.query('FIELD'):
            tags = ezdxf.lldxf.tags.Tags(ent.tags)

            # 'ACFD_FIELDTEXT_CHECKSUM'のあるグループコードだけを
            # チェックすれば良い？
            if tags.get_first_value(6) != 'ACFD_FIELDTEXT_CHECKSUM':
                continue

            tag_list = self.make_tag_list(tags)
            for tag in tag_list:
                self.handle_walk(tag.value)
            self.logs += '=== field(Group Code) end. ===\n'
        
        with open(dxffile.replace('.dxf', '.txt'), 'w') as f:
            f.write(self.logs)

if __name__ == '__main__':
    pass