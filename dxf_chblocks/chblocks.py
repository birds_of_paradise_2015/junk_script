# coding: utf-8
""" 
ブロック内のエンティティの属性を変更する。

-----

動作確認：
    Python 3.7.3
    ezdxf 0.9
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import ezdxf
from ezdxf.lldxf.const import (BYBLOCK,
                               BYLAYER,
                               BYOBJECT,
                               LINEWEIGHT_BYLAYER,
                               LINEWEIGHT_BYBLOCK,
                               LINEWEIGHT_DEFAULT)


class ChBlocks(object):
    """ ブロック内のエンティティの属性を変更する。 """

    def __init__(self):
        """ 初期化 """
        self.dwg = None
        self.save_file = None

    def change_attrib(self, entity):
        """ 属性を変更する """
        # 色
        # print(f'  1==> COLOR: {entity.dxf.color}')
        entity.dxf.color = BYBLOCK
        # 線種
        # print(f'  2==> LINE TYPE: {entity.dxf.linetype}')
        entity.dxf.linetype = 'BYBLOCK'
        try:
            # 線の太さ
            # print(f'  3==> LINE WEIGHT: {entity.dxf.lineweight}')
            entity.dxf.lineweight = LINEWEIGHT_BYBLOCK
        except ValueError:
            pass 

    def change_blocks(self, dxffile):
        """ ブロックを変更する """
        self.dwg = ezdxf.readfile(dxffile)

        # 特定のブロックだけ変更する場合は、
        # BlocksSection.get(name, default=None)
        # EntityQuery.query(query='*')
        # を使用する。
        # https://ezdxf.mozman.at/docs/blocks/blocks.html
        # https://ezdxf.mozman.at/docs/query.html
        for block in self.dwg.blocks:
            # print(f'BLOCK NAME: {block.name}')

            # モデル空間、ペーパー空間は除外する。
            if block.name in ('*Model_Space', '*Paper_Space'):
                continue

            for entity in block:
                self.change_attrib(entity)

        self.save_file = dxffile.replace('.dxf', '_after.dxf')
        self.dwg.saveas(self.save_file)


if __name__ == '__main__':
    pass
