# coding: utf-8
""" 
図面内の寸法をExcelに書き出す。

-----

動作確認：
    Python 3.7.4
    ezdxf 0.9
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import ezdxf
from openpyxl import Workbook

from attribs import column_map

class  CollectDimXL(object):
    """ 図面内の寸法をExcelに書き出す。 """

    def __init__(self):
        """ 初期化 """
        self.dwg = None
        self.xlfile = None

    def collect_dim_xl(self, dxffile):
        """ ブロックを変更する """
        self.dwg = ezdxf.readfile(dxffile)

        self.xlfile = dxffile.replace('.dxf', '.xlsx')
        wb = Workbook()
        ws1 = wb.active
        ws1.title = '寸法値リスト'

        # print(column_map)

        row = 2
        for name in column_map.keys():
            ws1.cell(column=column_map[name], row=1, value=name)

        dims = self.dwg.query('DIMENSION')
        for dim in dims:
            all_attribs = dim.dxfattribs()
            # print(f'HANDLE: {dim.dxf.handle}, ACTUAL MEASUREMENT: {dim.dxf.actual_measurement}')
            
            for name in column_map.keys():
                if name not in all_attribs:
                    # print(f'not found {name}.')
                    continue
                attrib_value = f'{all_attribs[name]}' if type(all_attribs[name]) == tuple else all_attribs[name]
                ws1.cell(column=column_map[name], row=row, value=attrib_value)

            row += 1

        wb.save(self.xlfile)
        print('process end.')

if __name__ == '__main__':
    pass
