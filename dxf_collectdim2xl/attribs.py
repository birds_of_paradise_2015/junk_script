attribs = (
    'handle',
    'owner',

    'paperspace',
    'layer',
    'linetype',
    'lineweight',
    'ltscale',
    'invisible',
    'color',
    'true_color',
    'color_name',
    'transparency',
    'shadow_mode',

    'geometry',
    'dimstyle',
    'defpoint',
    'text_midpoint',
    'insert',
    'dimtype',
    'text',
    'defpoint2',
    'defpoint3',
    'defpoint4',
    'defpoint5',
    'leader_length',
    'angle',
    'horizontal_direction',
    'oblique_angle',
    'text_rotation',
    'attachment_point',
    'line_spacing_style',
    'line_spacing_factor',
    'actual_measurement',
    'extrusion',
)






LINEAR = 0                    # 0b00000000
ALIGNED = 1                   # 0b00000001
ANGULAR = 2                   # 0b00000010
DIAMETER = 3                  # 0b00000011
RADIUS = 4                    # 0b00000100
ANGULAR_3P = 5                # 0b00000101
ORDINATE = 6                  # 0b00000110
BLOCK_EXCLUSIVE = 32          # 0b00100000
ORDINATE_TYPE = 64            # 0b01000000
USER_LOCATION_OVERRIDE = 128  # 0b10000000

column_map = {name: i for i, name in enumerate(attribs, 1)}
    
# print(column_map)
