<# Encoding: utf8
  DXF ファイルを削除する。

  ##############################################################################

  The MIT License (MIT)

  Copyright (c) 2021 極楽鳥(bird_of_paradise)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
#>
$jsonFile = Join-Path $PSScriptRoot "hex_setting.json"

# JSON ファイルを読み込む。
if (Test-Path $jsonFile) {
  $setting = (Get-Content -Path $jsonFile | ConvertFrom-Json)
  $tempFolder = $setting.tempFolder
}

$in_dxf = Join-Path $tempFolder "hex1.dxf"
# $out_dxf = Join-Path $tempFolder "hex2.dxf"

if (Test-Path $in_dxf) {
  Remove-Item -Path $in_dxf  
}
