# hex_counter

１６進数カウンタ

---

## DIESELマクロ

１６進数カウンタの開始番号と作業フォルダを設定する設定マクロと１６進数を書き込む実行マクロの２つのマクロで構成されています。

---

### 例として使うフォルダ

以下のファイル構成になっていること前提に話を勧めていきます。

- 作業フォルダ：C:\TEMP
- 処理前DXFファイル：C:\TEMP\HEX1.DXF
- 処理後DXFファイル：C:\TEMP\HEX2.DXF
- 全てのスクリプトファイルにはAutoCADのサポートファイルの検索パスが通っているものとします。

---

### 設定マクロ

「開始番号」と「作業フォルダ」の情報を設定する。

LT版：
```
^C^CAI_STARTAPP;"hex_setting.bat"^M
```

Release版：
```
^C^C(STARTAPP "hex_setting.bat")^Z
```

---

### 実行マクロ

以下４つからなるDIESELマクロをそれぞれツールバーなどに登録する。

手順を順番通りに必要回数分繰り返す。

#### 手順(1) ポインティングからDXF書き出し
```
^C^CTEXT;$M=$(if,$(getvar,cmdactive),\)2.5;0;%HEXTEXT%;_DXFOUT;C:/TEMP/HEX1.DXF;O;L;;V;2013;16^M
```
#### 手順(2) １６進数カウンタ実行
LT版：
```
^C^CAI_STARTAPP;"hex_counter.bat"^M
```
Release版：
```
^C^C(STARTAPP "hex_counter.bat")^Z
```
#### 手順(3) 置換DXF挿入
```
^C^C_ERASE;L;;-INSERT;C:/TEMP/HEX2.DXF;0,0;1;1;0;_EXPLODE;L;-PURGE;B;HEX2;N^M
```
#### 手順(4) 一時作業ファイル削除
LT版：
```
^C^CAI_STARTAPP;"clean.bat"^M
```
Release版：
```
^C^C(STARTAPP "clean.bat")^Z
```

---

### 実行マクロ（LT版 ひとまとめ）
動く？

```
^C^CTEXT;$M=$(if,$(getvar,cmdactive),\)2.5;0;%HEXTEXT%;_DXFOUT;C:/TEMP/HEX1.DXF;O;L;;V;2013;16;AI_STARTAPP;"hex_counter.bat";SCRIPT;delay.scr;_ERASE;L;;-INSERT;C:/TEMP/HEX2.DXF;0,0;1;1;0;EXPLODE;L;-PURGE;B;HEX2;N;AI_STARTAPP;"clean.bat";SCRIPT;delay.scr^M
```

---

## 前提

- 設定で「作業フォルダ」に入力するフォルダのパスと実行マクロの書き込むDXFファイルのフォルダは同じフォルダにしなければならない。
- 必要であれば、DIESELマクロとPowerShellのコードは使用する環境に合わせて適宜修正する。
- AutoCAD上でポイントしてから次をポイントするまでに数秒の間が必要です。

---

## ファイル

### hex_setting.ps1
設定用スクリプト。

### hex_setting.json
hex_setting.ps1を実行すると生成される。
「開始番号」と「作業フォルダ」の情報を保持。

### hex_counter.ps1
１６進数カウンタ本体スクリプト。

### delay.scr
遅延時間を設定するスクリプト。

### clean.ps1
DXFファイル削除用スクリプト。

### *.bat
PowerShellスクリプトを実行した際に「PSSecurityException」が出る場合に使用する。

---

## 基本ワークフロー（イメージ）

「開始番号」と「作業フォルダ」を設定(hex_setting.ps1)

👇

適当なテキスト(%HEXTEXT%)を入力(AutoCAD)　※繰り返し開始ポイント

👇

DXFファイルに書き出し(AutoCAD)

👇

１６進数カウンタ実行(hex_counter.ps1)

👇

DXFファイルを書き換え(hex_counter.ps1)

👇

１６進数カウンタの値を加算(hex_counter.ps1)

👇

DXFファイルを挿入(AutoCAD)

👇

DXFファイル削除(clean.ps1)

👇

必要回数分繰り返す
