<# Encoding: utf8
  １６進数カウンタ
  
  PowerShell 5.1 以前で日本語を表示するコードを書く場合は SHIFT JIS で書く。

  「PSSecurityException」が出てスクリプトが実行できない場合は、
  「PowerShell.exe -ExecutionPolicy RemoteSigned .\hex_counter.ps1」
  で実行する。

  ##############################################################################

  The MIT License (MIT)

  Copyright (c) 2021 極楽鳥(bird_of_paradise)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
#>
# JSON ファイルを読み込む。
$jsonFile = Join-Path $PSScriptRoot "hex_setting.json"
if (Test-Path $jsonFile) {
  $setting = (Get-Content -Path $jsonFile | ConvertFrom-Json)
  $in_str = $setting.inHex
  $hexUpperCase = $setting.UpperCase
  $tempFolder = $setting.tempFolder
}

# 処理する DXF ファイルのパス。
try {
  $in_dxf = Join-Path $tempFolder "hex1.dxf"
  $out_dxf = Join-Path $tempFolder "hex2.dxf" 
}
catch {
  Write-Error "Can't join dxf file path."
  exit 1
}

try {
  $int_hex = [Convert]::ToInt32($in_str, 16)  
  $numFmtStr = if ($hexUpperCase) { "X" } else { "x" }
  $hex_fotmat = $numFmtStr + $in_str.Length.ToString()
  $out_hex = $int_hex.ToString($hex_fotmat)
  $int_hex++
    
}
catch {
  $out_hex = "value error."

  # カウンタの値を0リセットする。
  $int_hex = 0
  $hexUpperCase = $true
  $hex_fotmat = "X2"

  exit 1

}
finally {
  # DXF ファイルの内容書き換え。
  # Encoding;
  #   2007 以降：utf8NoBom
  #   2007 以前：default

  if ($PSVersionTable.PSVersion.Major -ge 6) {
    Get-Content -Encoding utf8NoBom -path $in_dxf | ForEach-Object { $_ -Replace '%HEXTEXT%', $out_hex } |  Out-File -Encoding utf8NoBom $out_dxf

  }
  else {
    # DXF ファイルは BOM なしUTF-8 のテキストファイル。
    # Out-File の -Encoding に BOM なしUTF-8 は指定できないので、
    # 以下のような構文で BOM なしUTF-8 の DXF ファイルを作成する。
    # 基本構文：
    # $MyFile = Get-Content $MyPath
    # $Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False
    # [System.IO.File]::WriteAllLines($MyPath, $MyFile, $Utf8NoBomEncoding)

    $MyFile = (Get-Content -Encoding utf8 -path $in_dxf | ForEach-Object { $_ -Replace '%HEXTEXT%', $out_hex })
    $Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False
    [System.IO.File]::WriteAllLines($out_dxf, $MyFile, $Utf8NoBomEncoding)
    
  }

  # JSON データの更新と保存
  $json = @{inHex = ($int_hex.ToString($hex_fotmat)); UpperCase = $hexUpperCase; tempFolder = $tempFolder}
  ConvertTo-Json $json | Out-File $jsonFile -Encoding utf8

}
