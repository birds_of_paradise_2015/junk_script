<# Encoding: shift_jis
  １６進数カウンタ用設定ダイアログ

  ##############################################################################

  The MIT License (MIT)

  Copyright (c) 2021 極楽鳥(bird_of_paradise)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
#>
Add-Type -AssemblyName PresentationFramework
[xml]$xaml = @'
<Window xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
  xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" Title="設定"
  Width="220" Height="180">
<StackPanel>
  <TextBlock Text="開始番号（１６進数で）：" Margin="0,10,0,0" />
  <TextBox Name="inHex" Text=""/>
  <TextBlock Text="作業フォルダ：" Margin="0,10,0,0" />
  <TextBox Name="tempFolder" Text=""/>
  <TextBlock Name="errerText" Text="" Foreground="Red" Margin="0,10,0,0" />
  <Button Name="okButton" Content="OK" IsDefault="True" Margin="0,10,0,0" />
</StackPanel>
</Window>
'@
$reader = (New-Object System.Xml.XmlNodeReader $xaml)
$window = [Windows.Markup.XamlReader]::Load($reader)

$jsonFile = Join-Path $PSScriptRoot "hex_setting.json"

$inHex = $window.FindName("inHex")
$tempFolder = $window.FindName("tempFolder")
$errerText = $window.FindName("errerText")
$okButton = $window.FindName("okButton")

# JSON ファイルがあればそれを読み込む。
if (Test-Path $jsonFile) {
  $setting = (Get-Content -Path $jsonFile | ConvertFrom-Json)
  $inHex.Text = $setting.inHex
  $tempFolder.Text = $setting.tempFolder
}

# 英字大文字で出力するかどうか。
$hexUpperCase = $false

# OKボタンが押された時の処理
$okButton_clicked = $okButton.add_Click
$okButton_clicked.Invoke({
  # 入力のチェック
  if ($inHex.Text -inotmatch  '^[0-9A-F]+$') {
    $errerText.Text = "１６進数ではありません。"
    return
  }

  # 英字大文字が含まれていれば、英字大文字で統一して出力する。
  if ($inHex.Text -cmatch  '[A-F]') {
    $hexUpperCase = $true
  }

  if (($tempFolder.Text -eq "") -or -not (Test-Path $tempFolder.Text)) {
    $errerText.Text = "作業フォルダが存在しません。"
    return
  }

  # JSON データの作成と保存
  $json = @{inHex = $inHex.Text; UpperCase = $hexUpperCase; tempFolder = $tempFolder.Text}
  ConvertTo-Json $json | Out-File $jsonFile -Encoding utf8

  $window.Close()
})

$window.ShowDialog() > $null
