#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# Support module generated by PAGE version 4.13
# In conjunction with Tcl version 8.6
#    May 30, 2018 05:08:17 PM

import sys

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

from pathlib import Path

import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox

def browse():
    """ファイル選択用ダイアログボックスを表示する。"""

    global w, root

    # 作業するファイルダイアログボックス用オプション設定
    base_dir = Path(__file__).resolve().parent
    file_opt = options = {}
    options['filetypes'] = [('DXFファイル', '*.dxf')]
    options['initialdir'] = base_dir
    options['parent'] = root
    options['title'] = '作業するファイルの選択'

    directory = tkinter.filedialog.askopenfilename(**file_opt)
    w.TEntry1.delete(0, tk.END)
    w.TEntry1.insert(tk.END, directory)

    # ファイルパスをツールチップで表示する。
    from delete_layouts_gui import ToolTip
    ToolTip(w.TEntry1, "TkDefaultFont", directory, delay=0.5)

def run():
    """実行部。"""

    global w

    dxffile = w.TEntry1.get()
    if not Path(dxffile).exists():
        tkinter.messagebox.showwarning('警告', f'"{dxffile}"\n\nファイルが見つかりません。')
        return

    from delete_layouts import DeleteLayouts
    try:
        op_dxf = DeleteLayouts(dxffile)
        op_dxf.delete_layouts()
        op_dxf.saveas()
    except Exception as e:
        tkinter.messagebox.showinfo('情報', f'エラーが発生しました。\n\n{e}')
        return

    tkinter.messagebox.showinfo('情報', f'処理を終了しました。\n\n{op_dxf.save_file}に書き込みました。')

def quit():
    destroy_window()

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

if __name__ == '__main__':
    import replace_block_name_gui
    replace_block_name_gui.vp_start_gui()




