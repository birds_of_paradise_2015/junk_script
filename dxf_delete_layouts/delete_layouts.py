# coding: utf-8
""" 
指定したレイアウトタブを削除するサンプルコード。

-----

動作確認：
    Python 3.8.0
    ezdxf 0.10.2
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import sys
from pathlib import Path
import ezdxf

class DeleteLayouts(object):
    """ 指定したレイアウトタブを削除する。

    """

    def __init__(self, dxffile: str) -> None:
        """ 初期化 """
        self.dwg = ezdxf.readfile(dxffile)
        self.save_file = None

    def save(self) -> None:
        """ 図面を保存する """
        self.dwg.save()

    def saveas(self) -> None:
        """ 図面を名前を付けて保存する """
        """ 今のところ末尾に「_after」を足すだけ """
        self.save_file = self.dwg.filename.replace('.dxf', '_after.dxf')
        self.dwg.saveas(self.save_file)

    def delete_layouts(self) -> None:
        """ 指定したレイアウトタブを削除する。本体。 """

        for i, tab in enumerate(self.dwg.layouts.names_in_taborder()):
            print(f'{i:02}: {tab}')
            # レイアウトタブの2ページ目と5ページ目以降を削除する。
            if i == 1 or i >= 4:
                try:
                    self.dwg.layouts.delete(tab)
                    print('    ---> 削除。')
                except ValueError:
                    print('    ---> 「モデル」タブは削除できません。')
                except KeyError:  # このサンプルでは発生しない。
                    print(f'    ---> 「{tab}」タブは存在しません。')


if __name__ == '__main__':
    dxffile = Path(sys.argv[1])
    ad_dxf = DeleteLayouts(str(dxffile.resolve()))
    ad_dxf.delete_layouts()
