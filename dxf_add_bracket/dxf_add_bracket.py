# coding: utf-8
""" 
選択されたテキストに対して、括弧を追加します。

-----

動作確認：
    Python 3.8.0
    ezdxf 0.10.2
################################################################################

The MIT License (MIT)

Copyright (c) 2019 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import sys
from pathlib import Path
# from pprint import pprint
import ezdxf

class AddBracket(object):
    """ 選択されたテキストに対して、括弧を追加します。

    """

    def __init__(self, dxffile: str) -> None:
        """ 初期化 """
        self.dwg = ezdxf.readfile(dxffile)

    def add_bracket(self) -> None:
        """ 選択されたテキストに対して、括弧を追加します。本体。 """

        dim_texts = self.dwg.query('TEXT')

        for dim_text in dim_texts:
            # pprint(dim_text.dxfattribs())
            dim_text.update_dxf_attribs({
                'text': f'({dim_text.dxf.text})',
                'layer': '追加'
                })

        self.dwg.save()


if __name__ == '__main__':
    # dxffile = Path(r'C:\temp\dxf_temp.dxf')
    dxffile = Path(sys.argv[1])
    ad_dxf = AddBracket(str(dxffile.resolve()))
    ad_dxf.add_bracket()
