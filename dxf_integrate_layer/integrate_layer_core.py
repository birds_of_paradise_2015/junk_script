# coding: utf-8
""" 
画層を統合する

-----

動作確認：
    Python 3.7.0
    ezdxf 0.8.8
################################################################################

The MIT License (MIT)

Copyright (c) 2018 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from collections import namedtuple
import ezdxf
from ezdxf.lldxf.const import (BYBLOCK,
                               BYLAYER,
                               BYOBJECT,
                               LINEWEIGHT_BYLAYER,
                               LINEWEIGHT_BYBLOCK,
                               LINEWEIGHT_DEFAULT)


class IntegrateLayer(object):
    """ 画層を統合する """

    def __init__(self):
        """ 初期化 """
        self.dwg = None
        self.layers = {}
        self.outer_layer_name = None
        self.save_file = None

    def make_layer_map(self):
        """ 画層の辞書を作成する
            都度図面を検索する代わりに作成した辞書から情報を取り出す。
        """
        # TODO: 他の属性、表示、フリーズ、ロックなどは含めるか？
        LayerCollections = namedtuple('LayerCollections', ('linetype', 'color', 'lineweight'))
        for layer in self.dwg.layers:
            self.layers[layer.dxf.name] = LayerCollections(linetype=layer.dxf.linetype,
                                                        #    color=abs(layer.dxf.color),
                                                           color=layer.dxf.color,
                                                           lineweight=layer.dxf.line_weight)

    def collect_block_name(self):
        """ モデル空間、ペーパー空間、ブロックの名前収集 """
        return [blk.name for blk in self.dwg.blocks]

    def print_entity_info(self, entity):
        """ エンティティの情報表示 """
        return f'TYPE: {entity.dxftype()}, LAYER: {entity.dxf.layer}, ' +\
               f'LINETYPE: {entity.dxf.linetype}, COLOR: {entity.dxf.color}, ' +\
               f'LINEWEIGHT: {entity.dxf.lineweight}'

    def change_attrib(self, entity):
        """ 属性を変更する """
        if entity.dxf.layer in ('0', 'Defpoints'):
            print('nothing to be done.')
            return

        # ブロックに属しているエンティティの場合、そのブロックの画層を参照する
        layer_name = self.outer_layer_name if self.outer_layer_name else entity.dxf.layer

        # 【注意！】属性はline_weightではなくて、lineweight
        # ない場合があるので……
        # TODO: エンティティごとに扱いを変えるべき？
        try:
            entity.supported_dxf_attrib('lineweight')
        except AttributeError:
            entity.set_dxf_attrib('lineweight', self.layers[entity.dxf.layer].lineweight)

        print(f'before: [{self.print_entity_info(entity)}]')

        # TODO: 他の属性の扱いはどうするか？

        # 線種
        if entity.dxf.linetype.upper() == 'BYLAYER':
            # 常にentity.dxf.layerを使う
            entity.dxf.linetype = self.layers[entity.dxf.layer].linetype
        elif entity.dxf.linetype.upper() == 'BYBLOCK':
            entity.dxf.linetype = self.layers[layer_name].linetype

        # 色
        if entity.dxf.color == BYLAYER:
            # 常にentity.dxf.layerを使う
            entity.dxf.color = self.layers[entity.dxf.layer].color
        elif entity.dxf.color == BYBLOCK:
            entity.dxf.color = self.layers[layer_name].color

        # 線の太さ
        if entity.dxf.lineweight == LINEWEIGHT_BYLAYER:
            # 常にentity.dxf.layerを使う
            entity.dxf.lineweight = self.layers[entity.dxf.layer].lineweight
        elif entity.dxf.lineweight == LINEWEIGHT_BYBLOCK:
            entity.dxf.lineweight = self.layers[layer_name].lineweight

        entity.dxf.layer = '0'

        print(f'after: [{self.print_entity_info(entity)}]')

    def integrate_layer(self, dxffile):
        """ 画層を合成する """
        self.dwg = ezdxf.readfile(dxffile)

        self.make_layer_map()

        for layout in self.dwg.layouts:
            # print(layout.name)
            for e in layout:
                # FIXME: 入れ子のINSERTの場合
                if e.dxftype() == 'INSERT':
                    print(f'=== in {e.dxftype()} [{e.dxf.name}, HANDLE: {e.dxf.handle}]')
                    # print(f'=x=> {e.dxf.layer}')
                    self.outer_layer_name = e.dxf.layer
                    # INSERTの中のエンティティを処理する
                    for e2 in self.dwg.blocks.get(e.dxf.name):
                        print(f'::: TYPE: {e2.dxftype()}, HANDLE: {e2.dxf.handle}')
                        self.change_attrib(e2)
                self.outer_layer_name = None
                print(f'=== TYPE: {e.dxftype()}, HANDLE: {e.dxf.handle}')
                self.change_attrib(e)

        # TODO: 不要になった画層の削除

        self.save_file = dxffile.replace('.dxf', '_after.dxf')
        self.dwg.saveas(self.save_file)


if __name__ == '__main__':
    pass
