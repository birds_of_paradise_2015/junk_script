# coding: utf-8
""" 
画層を統合する

-----

動作確認：
    Python 3.7.0
    ezdxf 0.8.8
################################################################################

The MIT License (MIT)

Copyright (c) 2018 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from collections import namedtuple
import ezdxf


class IntegrateLayer(object):
    """ 画層を統合する """

    def __init__(self):
        """ 初期化 """
        self.dwg = None
        self.layers = {}
        self.outer_layer_name = None
        self.save_file = None

    def make_layer_map(self):
        """ 画層の辞書を作成する
            都度図面を検索する代わりに作成した辞書から情報を取り出す。
        """
        LayerCollections = namedtuple('LayerCollections', ('linetype', 'color', 'lineweight'))
        for layer in self.dwg.layers:
            self.layers[layer.dxf.name] = LayerCollections(linetype=layer.dxf.linetype,
                                                           # color=abs(layer.dxf.color),
                                                           color=layer.dxf.color,
                                                           lineweight=layer.dxf.line_weight)
        # print(self.layers)

    def collect_block_name(self):
        """ モデル空間、ペーパー空間、ブロックの名前収集 """
        return [blk.name for blk in self.dwg.blocks]

    def change_attrib(self, entity):
        """ 属性を変更する """
        if entity.dxftype() == 'INSERT':
            # print(entity.dxf.name)
            self.outer_layer_name = entity.dxf.layer

        layer_name = self.outer_layer_name if self.outer_layer_name else entity.dxf.layer

        __entity_info = f'TYPE: {entity.dxftype()}, LAYER: {entity.dxf.layer}, ' +\
                        f'LINETYPE: {entity.dxf.linetype}, COLOR: {entity.dxf.color}'
        # 【注意！】属性はline_weightではなくて、lineweight
        __exist_lineweight = True
        try:
            __entity_info += f', LINEWEIGHT: {entity.dxf.lineweight}'
        except ezdxf.lldxf.const.DXFAttributeError:
            __exist_lineweight = False
        except ezdxf.lldxf.const.DXFValueError:
            __exist_lineweight = False
        print(f'before: [{__entity_info}]')

        if entity.dxf.layer in ('0', 'Defpoints'):
            print('nothing to do.')
            return
        
        if entity.dxf.linetype in ('BYLAYER', 'ByBlock'):
            entity.dxf.linetype = self.layers[layer_name].linetype
        
        if entity.dxf.color in (0, 256):  # BYBLOCK = 0, BYLAYER = 256
            entity.dxf.color = self.layers[layer_name].color
        
        if __exist_lineweight:
            if entity.dxf.lineweight in (-3, -2, -1):  # DEFAULT = -3, BYBLOCK = -2, BYLAYER = -1
                entity.dxf.lineweight = self.layers[layer_name].lineweight
        
        entity.dxf.layer = '0'

        __entity_info = f'TYPE: {entity.dxftype()}, LAYER: {entity.dxf.layer}, ' +\
                        f'LINETYPE: {entity.dxf.linetype}, COLOR: {entity.dxf.color}'
        if __exist_lineweight:
            __entity_info += f', LINEWEIGHT: {entity.dxf.lineweight}'
        print(f'after: [{__entity_info}]')

    def integrate_layer(self, dxffile):
        """ 画層を合成する """
        self.dwg = ezdxf.readfile(dxffile)

        self.make_layer_map()

        blocks = self.collect_block_name()
        print(f'blocks: {blocks}')

        for block in blocks:
            for e in self.dwg.blocks.get(block):
                self.change_attrib(e)

        self.save_file = dxffile.replace('.dxf', '_after.dxf')
        self.dwg.saveas(self.save_file)


if __name__ == '__main__':
    pass
