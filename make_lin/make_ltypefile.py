# -*- coding: UTF-8 -*-
""" 
DXFファイルから線種ファイルを作成する

-----

http://help.autodesk.com/view/OARX/2018/ENU/?guid=GUID-F57A316C-94A2-416C-8280-191E34B182AC
https://knowledge.autodesk.com/ja/search-result/caas/CloudHelp/cloudhelp/2018/JPN/AutoCAD-DXF/files/GUID-F57A316C-94A2-416C-8280-191E34B182AC-htm.html

-----

動作確認：
    Python 3.6.4
    ezdxf 0.8.8
################################################################################

The MIT License (MIT)

Copyright (c) 2018 極楽鳥(bird_of_paradise)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from __future__ import print_function

import os
import ezdxf

def shape_number_to_name(shapefile, shape_number):
    """
    シェイプファイルからシェイプ番号を調べてシェイプ名を返す。

    .shpファイルを検索して、ファイル内からシェイプ名を返す
    コードの実装にはなっていない。
    値を文字列で返すようになっている。

    TODO: .shpファイルを検索して、ファイル内からシェイプ名を返すコード
    """
    # ltypeshp.shpファイル内を検索する代わりに、
    # 辞書(ltypeshp)を用意して、対応するシェイプ名を取り出す。
    ltypeshp = {
        # シェイプ番号: シェイプ名
        130: 'TRACK1',
        131: 'ZIG',
        132: 'BOX',
        133: 'CIRC1',
        134: 'BAT',
        135: 'AMZIGZAG'
    }

    if shapefile.lower() == 'ltypeshp.shx':
        return ltypeshp[shape_number]
    else:
        return str(shape_number)

def make_ltypefile(dxffile):
    """
    DXFファイルから線種ファイルを作成する。
    """
    dwg = ezdxf.readfile(dxffile)

    # 線種ファイル名。
    # DXFファイル名と同じものとする。
    lin_file = os.path.splitext(dxffile)[0] + '.lin'

    # modelspace = dwg.modelspace()
    
    with open(lin_file, 'w') as lin:

        # ヘッダ部
        lin.write(';;\n')
        lin.write(f';; from \"{dxffile}\"\n')
        lin.write(';;\n')

        for linetype in dwg.linetypes:

            # 以下の線種名は処理から除外する。
            if linetype.dxf.name in ('ByLayer', 'ByBlock', 'Continuous'):
                continue

            # 線種名とその説明を書き込む。
            lin.write(f'*{linetype.dxf.name},{linetype.dxf.description}\n')

            # ここからグループコードを調べる。
            # 線種の定義のリスト
            linetype_pattern = ['A']
            # グループコード50の値が絶対的か、相対的か？
            # a = 絶対的
            # r = 相対的
            complex_rotation_string = 'r'
            # シェイプかどうか？
            is_shape = False
            # complex linetypeのリスト
            complex_pattern = []
            for tag in linetype.tags:
                if tag.code == 49:
                    linetype_pattern.append(f'{tag.value:g}')
                elif tag.code == 74:
                    if tag.value != 0:
                        #absolute rotation flag
                        complex_rotation_string = 'a' if (tag.value % 2) == 1 else 'r'
                    if len(complex_pattern) != 0:
                        if is_shape == True:
                            complex_pattern[0] = shape_number_to_name(complex_pattern[1], complex_pattern[0])
                        linetype_pattern.insert(-1, f'[{",".join(complex_pattern)}]')
                        is_shape = False
                        complex_pattern = []
                elif tag.code == 340:
                    style_name = ''
                    style = dwg.get_dxf_entity(tag.value)
                    if style.dxf.name != '':
                        style_name = style.dxf.name
                    else:
                        # TODO: もう少しマシなコードにする。
                        for style_tag in style.tags:
                            if style_tag.code == 3:
                                style_name = style_tag.value
                                break
                    complex_pattern.append(f'{style_name}')
                elif tag.code == 75 and tag.value != 0:
                    is_shape = True
                    complex_pattern.append(tag.value) # str型にしない。
                elif tag.code == 46:
                    complex_pattern.append(f's={tag.value}')
                elif tag.code == 50:
                    complex_pattern.append(f'{complex_rotation_string}={tag.value}')
                elif tag.code == 44:
                    complex_pattern.append(f'x={tag.value}')
                elif tag.code == 45:
                    complex_pattern.append(f'y={tag.value}')
                elif tag.code == 9:
                    complex_pattern.insert(0, f'\"{tag.value}\"')

                # print(f'Group Code: {tag.code}, Value: {tag.value}')

            # 線種の定義を書き込む。
            lin.write(','.join(linetype_pattern) + '\n')

if __name__ == '__main__':
    pass