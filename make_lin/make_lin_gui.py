#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# GUI module generated by PAGE version 4.24.1
#  in conjunction with Tcl version 8.6
#    Aug 10, 2019 09:07:46 PM JST  platform: Windows NT

import sys

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import make_lin_gui_support

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = tk.Tk()
    top = make_lin (root)
    make_lin_gui_support.init(root, top)
    root.mainloop()

w = None
def create_make_lin(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = tk.Toplevel (root)
    top = make_lin (w)
    make_lin_gui_support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_make_lin():
    global w
    w.destroy()
    w = None

class make_lin:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#d9d9d9' # X11 color: 'gray85'
        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.configure('.',font="TkDefaultFont")
        self.style.map('.',background=
            [('selected', _compcolor), ('active',_ana2color)])

        top.geometry("400x90+650+150")
        top.title("DXFファイルから線種ファイルを作成するプログラム")
        top.configure(background="#d9d9d9")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="black")

        self.TLabel1 = ttk.Label(top)
        self.TLabel1.place(relx=0.025, rely=0.111, height=19, width=93)
        self.TLabel1.configure(background="#d9d9d9")
        self.TLabel1.configure(foreground="#000000")
        self.TLabel1.configure(font="TkDefaultFont")
        self.TLabel1.configure(relief="flat")
        self.TLabel1.configure(text='''作業するファイル：''')

        self.TEntry1 = ttk.Entry(top)
        self.TEntry1.place(relx=0.025, rely=0.333, relheight=0.267
                , relwidth=0.95)
        self.TEntry1.configure(takefocus="")
        self.TEntry1.configure(cursor="ibeam")

        self.TButton1 = ttk.Button(top)
        self.TButton1.place(relx=0.025, rely=0.667, height=25, width=76)
        self.TButton1.configure(command=make_lin_gui_support.run)
        self.TButton1.configure(takefocus="")
        self.TButton1.configure(text='''実行''')

        self.TButton2 = ttk.Button(top)
        self.TButton2.place(relx=0.588, rely=0.667, height=25, width=76)
        self.TButton2.configure(command=make_lin_gui_support.browse)
        self.TButton2.configure(takefocus="")
        self.TButton2.configure(text='''参照''')

        self.TButton3 = ttk.Button(top)
        self.TButton3.place(relx=0.788, rely=0.667, height=25, width=76)
        self.TButton3.configure(command=make_lin_gui_support.quit)
        self.TButton3.configure(takefocus="")
        self.TButton3.configure(text='''閉じる''')

if __name__ == '__main__':
    vp_start_gui()





